package de.wetten.konfiguration;

public enum Modus {

	// Pro Wette wird ein Punkt als Einsatz abgezogen
	EINSATZ, //

	// Wetten sind kostenlos
	GRATIS; //
}
