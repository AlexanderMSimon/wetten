package de.wetten.konfiguration;

import de.wetten.in.auslesen.BetProzenteAusleser;
import de.wetten.in.auslesen.GewichtetProzenteAusleser;
import de.wetten.in.auslesen.GleichProzenteAusleser;
import de.wetten.in.auslesen.NateProzenteAusleser;
import de.wetten.in.auslesen.ProzenteAusleser;

public enum User {

	BET_AT_HOME("BET", new BetProzenteAusleser()), //
	NATE_SILVER("NATE", new NateProzenteAusleser()), //
	GEWICHTET("GEWI", new GewichtetProzenteAusleser()), //
	GLEICH("GLEI", new GleichProzenteAusleser()); //

	private String kuerzel;
	private ProzenteAusleser prozenteAusleser;

	private User(String kuerzel, ProzenteAusleser prozenteAusleser) {
		this.kuerzel = kuerzel;
		this.prozenteAusleser = prozenteAusleser;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public ProzenteAusleser getProzenteAusleser() {
		return prozenteAusleser;
	}
}