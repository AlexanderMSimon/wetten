package de.wetten.konfiguration;

import de.wetten.out.consume.AuswertungsConsumer;
import de.wetten.out.consume.ConsoleGesamtAuswertungsConsumer;
import de.wetten.out.consume.CsvAuswertungsConsumer;
import de.wetten.out.consume.CsvGesamtAuswertungsConsumer;
import de.wetten.out.consume.GesamtAuswertungsConsumer;

public class Konfigurationen {

	private Konfigurationen() {
	}

	// --- Saison und Spiel ---
	public static final String JAHR = "201920";

	public static int[] getSpieltage() {
		return new int[]{1, 2};
	}

	// --- Modus ---
	public static final Modus MODUS = Modus.GRATIS;

	// --- User ---
	public static final User AWKWARDNESS_USER = User.BET_AT_HOME;

	// ---> User in der User-Klasse konfigurieren

	// --- Consumer ---
	public static AuswertungsConsumer[] getAuswertungsConsumers() {
		return new AuswertungsConsumer[]{new CsvAuswertungsConsumer()};

	}
	public static GesamtAuswertungsConsumer[] getGesamtAuswertungsConsumers() {
		return new GesamtAuswertungsConsumer[]{new ConsoleGesamtAuswertungsConsumer(),
				new CsvGesamtAuswertungsConsumer()};
	}
}
