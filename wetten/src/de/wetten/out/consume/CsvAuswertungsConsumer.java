package de.wetten.out.consume;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.wetten.core.auswertungFachklassen.AuswertungAllg;
import de.wetten.core.auswertungFachklassen.SpielAuswertungAllg;
import de.wetten.konfiguration.User;
import de.wetten.out.util.CsvHelper;
import de.wetten.util.StringHelper;

public class CsvAuswertungsConsumer implements AuswertungsConsumer {

	@Override
	public void consume(AuswertungAllg auswertung) {

		erstelleCsv(auswertung);

	}

	private void erstelleCsv(AuswertungAllg auswertung) {
		File file = new File(".\\src\\resources\\out\\" + auswertung.getJahr() + "\\" + "Spieltag"
				+ auswertung.getSpieltag() + ".csv");

		if (file.getParentFile() != null) {
			file.getParentFile().mkdirs();
		}

		try (FileWriter writer = new FileWriter(file);) {
			infoToCsv(writer, auswertung);
			writer.flush();

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void infoToCsv(FileWriter writer, AuswertungAllg auswertung) throws IOException {

		for (SpielAuswertungAllg spiel : auswertung.getSpielauswertungen()) {

			CsvSpielAuswertungsConsumer consumer = new CsvSpielAuswertungsConsumer(spiel);
			consumer.infoToCsv(writer);

			writer.append(System.lineSeparator());
			writer.append(System.lineSeparator());
			writer.append(System.lineSeparator());
			writer.append(System.lineSeparator());
		}

		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());

		// Gesamtpunkte von User beiUser am Spieltag
		writer.append(CsvHelper.matrixToCsv(auswertung.getGesamtpunkteBeiUser()));

		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());

		// Gesamtpunkte am Spieltag pro User
		writer.append("Gesamt:");
		writer.append(System.lineSeparator());

		for (User user : User.values()) {
			writer.append(user.toString());
			writer.append(",");

			String format = StringHelper.doubleToString(auswertung.getGesamtpunkte().get(user));

			writer.append(format);
			writer.append(System.lineSeparator());
		}

	}

}
