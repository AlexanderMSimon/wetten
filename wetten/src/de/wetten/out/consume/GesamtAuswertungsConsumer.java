package de.wetten.out.consume;

import de.wetten.core.auswertungFachklassen.GesamtAuswertungAllg;

public interface GesamtAuswertungsConsumer {

	void consume(GesamtAuswertungAllg gesamtAuswertung);
}
