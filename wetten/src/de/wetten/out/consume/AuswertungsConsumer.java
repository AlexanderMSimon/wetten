package de.wetten.out.consume;

import de.wetten.core.auswertungFachklassen.AuswertungAllg;

public interface AuswertungsConsumer {

	void consume(AuswertungAllg auswertung);
}
