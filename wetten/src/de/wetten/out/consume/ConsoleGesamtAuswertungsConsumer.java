package de.wetten.out.consume;

import java.util.List;
import java.util.Map;

import de.wetten.core.auswertungFachklassen.GesamtAuswertungAllg;
import de.wetten.util.CollectionHelper;

public class ConsoleGesamtAuswertungsConsumer implements GesamtAuswertungsConsumer {

	@Override
	public void consume(GesamtAuswertungAllg gesamtAuswertung) {

		System.out.println(gesamtAuswertung.getGesamtpunkte());

		System.out.println(" ----------------------------- ");

		System.out.println(gesamtAuswertung.getGesamtpunkteBeiUser());

		System.out.println(" ----------------------------- ");
		System.out.println(" ----------------------------- ");

		// --- Awkwardness ---

		awkwardnessAusgeben(gesamtAuswertung);

	}

	private void awkwardnessAusgeben(GesamtAuswertungAllg gesamtAuswertung) {

		System.out.println();
		System.out.println();
		System.out.println("Awkwardness:");
		System.out.println();

		Map<Integer, Double> awkwardnessProSpieltag = gesamtAuswertung.getAwkwardnessProSpieltag();

		List<Integer> keysSortedByValues = CollectionHelper.getKeysSortedByValues(awkwardnessProSpieltag);

		for (Integer key : keysSortedByValues) {

			System.out.println("Spieltag " + key + ": --  " + awkwardnessProSpieltag.get(key));
		}

	}

}
