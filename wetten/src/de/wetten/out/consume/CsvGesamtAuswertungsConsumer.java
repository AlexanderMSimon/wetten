package de.wetten.out.consume;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.wetten.core.auswertungFachklassen.GesamtAuswertungAllg;
import de.wetten.konfiguration.User;
import de.wetten.out.util.CsvHelper;
import de.wetten.util.CollectionHelper;
import de.wetten.util.StringHelper;

public class CsvGesamtAuswertungsConsumer implements GesamtAuswertungsConsumer {

	@Override
	public void consume(GesamtAuswertungAllg gesamtAuswertung) {
		erstelleCsv(gesamtAuswertung);
	}

	private void erstelleCsv(GesamtAuswertungAllg gesamtAuswertung) {
		File file = new File(".\\src\\resources\\out\\" + gesamtAuswertung.getJahr() + "\\" + "Gesamt.csv");

		if (file.getParentFile() != null) {
			file.getParentFile().mkdirs();
		}

		try (FileWriter writer = new FileWriter(file);) {
			infoToCsv(writer, gesamtAuswertung);
			writer.flush();

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void infoToCsv(FileWriter writer, GesamtAuswertungAllg gesamtAuswertung) throws IOException {
		// Gesamtpunkte von User beiUser
		writer.append(CsvHelper.matrixToCsv(gesamtAuswertung.getGesamtpunkteBeiUser()));

		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());

		// Gesamtpunkte pro User
		writer.append("Gesamt:");
		writer.append(System.lineSeparator());

		for (User user : User.values()) {
			writer.append(user.toString());
			writer.append(",");

			String format = StringHelper.doubleToString(gesamtAuswertung.getGesamtpunkte().get(user));

			writer.append(format);
			writer.append(System.lineSeparator());
		}

		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());

		// awkwardness
		writer.append("Awkwardness:");
		writer.append(System.lineSeparator());

		Map<Integer, Double> awkwardnessProSpieltag = gesamtAuswertung.getAwkwardnessProSpieltag();

		List<Integer> keysSortedByValues = CollectionHelper.getKeysSortedByValues(awkwardnessProSpieltag);

		for (Integer key : keysSortedByValues) {
			writer.append("Spieltag " + key + ":," + awkwardnessProSpieltag.get(key));
			writer.append(System.lineSeparator());
		}
	}

}
