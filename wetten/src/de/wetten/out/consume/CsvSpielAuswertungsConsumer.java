package de.wetten.out.consume;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

import de.wetten.core.auswertungFachklassen.SpielAuswertungAllg;
import de.wetten.core.fachklassen.Resultat;
import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;
import de.wetten.util.StringHelper;

public class CsvSpielAuswertungsConsumer {

	private SpielAuswertungAllg auswertung;

	public CsvSpielAuswertungsConsumer(SpielAuswertungAllg auswertung) {
		this.auswertung = auswertung;
	}

	public void infoToCsv(FileWriter writer) throws IOException {

		// --- Kopf: Begegnung und Ergebnis ---
		writer.append(auswertung.getHeim().toString());
		writer.append(",");
		writer.append("-");
		writer.append(",");
		writer.append(auswertung.getGast().toString());
		writer.append(",");
		writer.append("Ergebnis:");
		writer.append(",");
		writer.append(auswertung.getResultat() == null ? "---" : auswertung.getResultat().toString());

		// --- Tabelle ---

		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());

		// Tabellen-Header
		writer.append("");
		writer.append(",");
		writer.append("1");
		writer.append(",");
		writer.append("");
		writer.append(",");
		writer.append("0");
		writer.append(",");
		writer.append("");
		writer.append(",");
		writer.append("2");
		writer.append(System.lineSeparator());

		for (User user : User.values()) {
			writer.append(user.toString());
			writer.append(",");

			getUserDieHieraufWetten(writer, user, Resultat.HEIM);
			getUserDieHieraufWetten(writer, user, Resultat.REMIS);
			getUserDieHieraufWetten(writer, user, Resultat.GAST);

			writer.append(System.lineSeparator());
		}

		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());
		writer.append(System.lineSeparator());

		// Gesamtpunkte pro User
		for (User user : User.values()) {
			writer.append(user.toString());
			writer.append(",");

			String format = StringHelper.doubleToString(auswertung.getGesamtpunkte().get(user));

			writer.append(format);
			writer.append(System.lineSeparator());
		}
	}

	private void getUserDieHieraufWetten(FileWriter writer, User user, Resultat resultat) throws IOException {

		Locale.setDefault(new Locale("en", "US"));
		DecimalFormat df = new DecimalFormat("#.00");

		Matrix<User, Resultat, String> wettMatrix = erstelleWettmatrix();

		writer.append("(" + auswertung.getProzente().get(user, resultat) + ")  ");
		writer.append(df.format(auswertung.getQuoten().get(user, resultat)));
		writer.append(",");

		// wer hat drauf gesetzt?

		String usernamen = wettMatrix.get(user, resultat);

		writer.append(usernamen == null ? "" : usernamen);

		writer.append(",");

	}

	// <beiUser, resultat, usernamen>
	private Matrix<User, Resultat, String> erstelleWettmatrix() {

		// matrix aufbauen

		Matrix<User, Resultat, String> wettMatrix = new Matrix<>();

		Matrix<User, User, Resultat> wetten = auswertung.getWetten();

		for (User user : wetten.keySet1()) {

			for (User beiUser : User.values()) {

				Resultat resultat = wetten.get(user, beiUser);
				wettMatrix.append(beiUser, resultat, user.getKuerzel(), (o, n) -> o + " " + n);

			}

		}
		return wettMatrix;
	}

}
