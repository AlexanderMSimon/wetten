package de.wetten.out.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.wetten.util.Matrix;

public class CsvHelper {

	private CsvHelper() {
	}

	// '| X Y Z
	// -+-------
	// A|
	// B|
	public static <K1, K2, V> String matrixToCsv(Matrix<K1, K2, V> matrix) {

		// A, B -- Liste, damit man eine Sortierung hat
		List<K1> labelErsteSpalte = new ArrayList<>(matrix.keySet1());

		// X, Y, Z
		Set<K2> labelTabellenkopfSet = new HashSet<>();

		for (K1 spaltenEintrag : labelErsteSpalte) {
			HashMap<K2, V> map = matrix.getByKey1(spaltenEintrag);

			labelTabellenkopfSet.addAll(map.keySet());
		}

		// Liste, damit man eine Sortierung hat
		List<K2> labelTabellenkopf = new ArrayList<>(labelTabellenkopfSet);

		StringBuilder strBui = new StringBuilder(",");

		for (K2 kopfEintrag : labelTabellenkopf) {
			strBui.append(kopfEintrag);
			strBui.append(",");
		}

		for (K1 spaltenEintrag : labelErsteSpalte) {
			strBui.append(System.getProperty("line.separator"));

			// label an der Zeile
			strBui.append(spaltenEintrag);
			strBui.append(",");

			// Eintraege in der Zeile
			for (K2 kopfEintrag : labelTabellenkopf) {
				V value = matrix.get(spaltenEintrag, kopfEintrag);
				strBui.append(value == null ? "" : value);
				strBui.append(",");
			}
		}

		return strBui.toString();
	}
}
