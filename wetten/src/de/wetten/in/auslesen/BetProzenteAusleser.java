package de.wetten.in.auslesen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import de.wetten.core.auswertung.ResultatComparator;
import de.wetten.core.fachklassen.Resultat;

public class BetProzenteAusleser implements ProzenteAusleser {

	@Override
	public Map<Resultat, Integer> prozenteAuslesen(ResourceBundle bundle, int spielNummer) {
		String betString = bundle.getString("spiel" + spielNummer + ".bet");

		String[] split = betString.split("\\|");

		if (split.length != 3) {
			throw new RuntimeException("Bet, Spielnummer " + spielNummer + " : etwas stimmt nicht");
		}

		return prozenteBerechnen(split[0].trim(), split[1].trim(), split[2].trim());
	}

	private Map<Resultat, Integer> prozenteBerechnen(String heim, String remis, String gast) {
		Map<Resultat, Integer> prozente = new HashMap<>();

		Double heimDouble = Double.valueOf(heim);
		Double remisDouble = Double.valueOf(remis);
		Double gastDouble = Double.valueOf(gast);

		double heimKehrwert = 1.0 / heimDouble;
		double remisKehrwert = 1.0 / remisDouble;
		double gastKehrwert = 1.0 / gastDouble;

		double faktor = heimKehrwert + remisKehrwert + gastKehrwert;

		Map<Resultat, Double> kehrwerte = new HashMap<>();
		kehrwerte.put(Resultat.HEIM, 100 * heimKehrwert / faktor);
		kehrwerte.put(Resultat.REMIS, 100 * remisKehrwert / faktor);
		kehrwerte.put(Resultat.GAST, 100 * gastKehrwert / faktor);

		// erst alle kehrwerte abrunden, dann die mit den groessten
		// Nachkommastellen hochzaehlen, bis die summe 100 ist
		List<Resultat> hochzaehlen = hochzaehlen(kehrwerte);

		for (Entry<Resultat, Double> entry : kehrwerte.entrySet()) {

			double heimProzent = Math.floor(entry.getValue());
			if (hochzaehlen.contains(entry.getKey())) {
				heimProzent++;
			}
			prozente.put(entry.getKey(), (int) heimProzent);
		}

		return prozente;
	}

	/**
	 * Ermittelt die Resultate, fuer die man eins hochzaehlen soll
	 */
	private List<Resultat> hochzaehlen(Map<Resultat, Double> map) {

		List<Resultat> zuErweiterm = new ArrayList<>();

		double summe = 0.0;
		Map<Resultat, Integer> nachkomma = new HashMap<>();

		for (Entry<Resultat, Double> entry : map.entrySet()) {

			// beachte nur ein paar Nachkommastellen
			double floor = Math.floor(entry.getValue());
			double nachkommastellen = entry.getValue() - floor;
			summe += floor;

			nachkomma.put(entry.getKey(), (int) Math.round(10000 * nachkommastellen));
		}

		for (int i = 0; i < Math.round(100.0 - summe); i++) {
			// ermittle Resultat mit groesstem Nachkommawert
			Resultat groesstes = getResultatMitGroesstemWert(nachkomma);
			zuErweiterm.add(groesstes);

			nachkomma.remove(groesstes);
		}

		return zuErweiterm;
	}

	/**
	 * Ermittelt Resultat mit groesstem Value.
	 */
	private Resultat getResultatMitGroesstemWert(Map<Resultat, Integer> map) {

		List<Resultat> maxResultate = filterMaxValue(map);

		if (maxResultate.size() == 1) {
			return maxResultate.get(0);
		}

		// mehr als ein Resultat mit dem maximalen Value

		maxResultate.sort(new ResultatComparator());

		return maxResultate.get(0);
	}

	/**
	 * Filtert alle Eintraege aus, deren Value nicht maximal ist.
	 */
	private List<Resultat> filterMaxValue(Map<Resultat, Integer> map) {
		int maxValue = map.entrySet().stream().map(e -> e.getValue()).mapToInt(Integer::intValue).max()
				.orElseThrow(() -> new RuntimeException("stsr"));

		List<Resultat> maxResultate = map.entrySet().stream().filter(e -> e.getValue() == maxValue).map(e -> e.getKey())
				.collect(Collectors.toList());
		return maxResultate;
	}

}
