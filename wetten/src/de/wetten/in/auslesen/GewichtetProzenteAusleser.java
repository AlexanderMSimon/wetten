package de.wetten.in.auslesen;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import de.wetten.core.fachklassen.Resultat;

public class GewichtetProzenteAusleser implements ProzenteAusleser {

	@Override
	public Map<Resultat, Integer> prozenteAuslesen(ResourceBundle bundle, int spielNummer) {

		Map<Resultat, Integer> prozente = new HashMap<>();
		prozente.put(Resultat.HEIM, 46);
		prozente.put(Resultat.REMIS, 25);
		prozente.put(Resultat.GAST, 29);

		return prozente;
	}
}
