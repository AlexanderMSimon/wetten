package de.wetten.in.auslesen;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import de.wetten.core.fachklassen.Resultat;

public class NateProzenteAusleser implements ProzenteAusleser {

	// passe immer heim nach oben oder unten an, falls die Summe nicht 100
	// ergibt
	private static final int AENDERN = 0;

	@Override
	public Map<Resultat, Integer> prozenteAuslesen(ResourceBundle bundle, int spielNummer) {
		String nateString = bundle.getString("spiel" + spielNummer + ".nate");

		String[] split = nateString.split("\\|");

		if (split.length != 3) {
			throw new RuntimeException("Nate, Spielnummer " + spielNummer + " : etwas stimmt nicht");
		}

		List<Integer> prozentListe = Arrays.stream(split).map(s -> Integer.valueOf(s.trim()))
				.collect(Collectors.toList());

		prozenteRunden(prozentListe);

		Map<Resultat, Integer> prozente = new HashMap<>();
		prozente.put(Resultat.HEIM, prozentListe.get(0));
		prozente.put(Resultat.REMIS, prozentListe.get(1));
		prozente.put(Resultat.GAST, prozentListe.get(2));

		return prozente;
	}

	private void prozenteRunden(List<Integer> prozentListe) {

		int gesamt = 0;
		for (Integer prz : prozentListe) {
			gesamt += prz;
		}

		if (gesamt == 99) {
			Integer wert = prozentListe.get(AENDERN);
			prozentListe.set(AENDERN, wert + 1);

		} else if (gesamt == 101) {
			Integer wert = prozentListe.get(AENDERN);
			prozentListe.set(AENDERN, wert - 1);

		} else if (gesamt != 100) {
			throw new RuntimeException("Nate: Die Prozente weichen zu stark von 100 ab");
		}
	}

}
