package de.wetten.in.auslesen;

import java.util.Map;
import java.util.ResourceBundle;

import de.wetten.core.fachklassen.Resultat;

public interface ProzenteAusleser {

	Map<Resultat, Integer> prozenteAuslesen(ResourceBundle bundle, int spielNummer);
}
