package de.wetten.in.auslesen;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import de.wetten.core.fachklassen.Resultat;

public class GleichProzenteAusleser implements ProzenteAusleser {

	@Override
	public Map<Resultat, Integer> prozenteAuslesen(ResourceBundle bundle, int spielNummer) {

		Map<Resultat, Integer> prozente = new HashMap<>();
		prozente.put(Resultat.HEIM, 34);
		prozente.put(Resultat.REMIS, 33);
		prozente.put(Resultat.GAST, 33);

		return prozente;
	}
}
