package de.wetten.in.init;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import de.wetten.core.fachklassen.Resultat;
import de.wetten.core.fachklassen.Verein;
import de.wetten.core.spiel.Spiel;
import de.wetten.core.spiel.SpielAllg;
import de.wetten.core.spieltag.Spieltag;
import de.wetten.core.spieltag.SpieltagAllg;
import de.wetten.in.resourcebundle.ResourceBundleBuilder;
import de.wetten.konfiguration.User;

public class BundesligaReader implements SpieltagInitiator {

	private String jahr;
	private int spieltag;

	private List<SpielAllg> spiele = new ArrayList<>();

	public BundesligaReader(String jahr, int spieltag) {
		this.jahr = jahr;
		this.spieltag = spieltag;

		spieleAuslesen();
	}

	@Override
	public SpieltagAllg init() {
		SpieltagAllg spieltag = new Spieltag();

		spieltag.setJahr(jahr);
		spieltag.setSpieltag(this.spieltag);
		spieltag.setSpiele(spiele);

		return spieltag;
	}

	private void spieleAuslesen() {

		String rbName = jahr + "_" + String.valueOf(spieltag);
		String directory = ".\\src\\resources\\in";
		ResourceBundleBuilder rbb = new ResourceBundleBuilder(rbName, directory);

		ResourceBundle bundle = rbb.build();

		for (int spielNummer = 1; spielNummer <= 9; spielNummer++) {

			SpielAllg spiel = new Spiel();

			setzeBegegnung(bundle, spielNummer, spiel);
			setzeResultat(bundle, spielNummer, spiel);

			// setze Prozente
			for (User user : User.values()) {

				Map<Resultat, Integer> prozentMap = user.getProzenteAusleser().prozenteAuslesen(bundle, spielNummer);

				prozentMap.entrySet().stream().forEach(e -> spiel.setProzent(user, e.getKey(), e.getValue()));
			}

			spiele.add(spiel);
		}
	}

	private void setzeBegegnung(ResourceBundle bundle, int spielNummer, SpielAllg spiel) {
		String begegnung = bundle.getString("spiel" + spielNummer);

		String[] split = begegnung.split("-");

		if (split.length != 2) {
			throw new RuntimeException("Begegnung, Spielnummer " + spielNummer + " : etwas stimmt nicht");
		}

		spiel.setBegegnung(Verein.getByName(split[0]), Verein.getByName(split[1]));
	}

	private void setzeResultat(ResourceBundle bundle, int spielNummer, SpielAllg spiel) {
		String resultatString = bundle.getString("spiel" + spielNummer + ".resultat");
		spiel.setResultat(Resultat.getByName(resultatString));
	}
}
