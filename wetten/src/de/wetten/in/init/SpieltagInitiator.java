package de.wetten.in.init;

import de.wetten.core.spieltag.SpieltagAllg;

public interface SpieltagInitiator {

	SpieltagAllg init();

}
