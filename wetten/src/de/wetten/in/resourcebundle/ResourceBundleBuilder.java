package de.wetten.in.resourcebundle;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleBuilder {

	private String bundleName;
	private String directory;

	/**
	 * Directory ist die Oberdirectory. Es wird genau eine Ebene drunter
	 * gesucht.
	 */
	public ResourceBundleBuilder(String bundleName, String directory) {
		this.bundleName = bundleName;
		this.directory = directory;
	}

	public ResourceBundle build() {

		ClassLoader loader = null;
		try {
			File[] directories = new File(directory).listFiles(File::isDirectory);

			int i = 0;
			URL[] urls = new URL[directories.length];

			for (File directory : directories) {
				urls[i] = directory.toURI().toURL();
				i++;
			}

			loader = new URLClassLoader(urls);

		} catch (MalformedURLException e) {
			// kann eigentl nicht passieren
			throw new RuntimeException("Das haette nicht passieren duerfen" + e.getMessage());
		}

		ResourceBundle bundle = ResourceBundle.getBundle(bundleName, Locale.getDefault(), loader);
		return bundle;

	}

}
