package de.wetten.core.spiel;

import de.wetten.core.auswertungFachklassen.SpielAuswertungAllg;
import de.wetten.core.fachklassen.Resultat;
import de.wetten.core.fachklassen.Verein;
import de.wetten.konfiguration.User;

public interface SpielAllg {

	/**
	 * Setze Grunddaten
	 */
	void setBegegnung(Verein heim, Verein gast);

	/**
	 * Setze das Resultat
	 */
	void setResultat(Resultat resultat);

	/**
	 * Setze die Prozentzahlen fuer einen User
	 */
	void setProzent(User user, Resultat resultat, int prozent);

	/**
	 * Ermittelt alle Informationen zu diesem Spiel
	 */
	SpielAuswertungAllg auswerten();

}
