package de.wetten.core.spiel;

import de.wetten.core.auswertung.SpielAuswertungBuilder;
import de.wetten.core.auswertungFachklassen.SpielAuswertungAllg;
import de.wetten.core.fachklassen.Resultat;
import de.wetten.core.fachklassen.Verein;
import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public class Spiel implements SpielAllg {

	private Verein heim;
	private Verein gast;
	private Resultat resultat;

	private Matrix<User, Resultat, Integer> prozentMatrix = new Matrix<>();

	// --- Interface ---
	@Override
	public void setBegegnung(Verein heim, Verein gast) {
		this.heim = heim;
		this.gast = gast;
	}

	@Override
	public void setResultat(Resultat resultat) {
		this.resultat = resultat;
	}

	@Override
	public void setProzent(User user, Resultat resultat, int prozent) {
		prozentMatrix.put(user, resultat, prozent);
	}

	@Override
	public SpielAuswertungAllg auswerten() {
		return new SpielAuswertungBuilder().setHeim(heim).setGast(gast).setResultat(resultat).setProzente(prozentMatrix)
				.build();
	}

}
