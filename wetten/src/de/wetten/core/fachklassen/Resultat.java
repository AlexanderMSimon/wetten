package de.wetten.core.fachklassen;

public enum Resultat {
	HEIM("1"), REMIS("0"), GAST("2");

	private String code;
	private Resultat(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public static Resultat getByName(String name) {

		for (Resultat resultat : Resultat.values()) {
			if (resultat.name().toLowerCase().equals(name.trim().toLowerCase())) {
				return resultat;
			}
		}
		return null;

	}

}
