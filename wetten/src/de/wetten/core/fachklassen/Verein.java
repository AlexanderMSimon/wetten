package de.wetten.core.fachklassen;

public enum Verein {
	FCB, LEIPZIG, BVB, HOFFENHEIM, HERTHA, FRANKFURT, KOELN, FREIBURG, GLADBACH, //
	LEV, S04, MAINZ, AUGSBURG, WOLFSBURG, WERDER, HSV, INGOLSTADT, DARMSTADT, VFB, H96, //
	DDORF, PADERB, UNION;

	public static Verein getByName(String name) {
		for (Verein verein : Verein.values()) {
			if (verein.name().toLowerCase().equals(name.trim().toLowerCase())) {
				return verein;
			}
		}
		throw new RuntimeException("Unbekannter Verein");
	}

}
