package de.wetten.core.spieltag;

import java.util.List;

import de.wetten.core.auswertungFachklassen.AuswertungAllg;
import de.wetten.core.spiel.SpielAllg;

public interface SpieltagAllg {

	void setJahr(String jahr);

	void setSpieltag(int spieltag);

	void setSpiele(List<SpielAllg> spiele);

	AuswertungAllg auswerten();
}
