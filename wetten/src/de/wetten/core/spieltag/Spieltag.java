package de.wetten.core.spieltag;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import de.wetten.core.auswertungFachklassen.Auswertung;
import de.wetten.core.auswertungFachklassen.AuswertungAllg;
import de.wetten.core.auswertungFachklassen.SpielAuswertungAllg;
import de.wetten.core.spiel.SpielAllg;
import de.wetten.konfiguration.User;
import de.wetten.util.EnhancedMap;
import de.wetten.util.Matrix;

public class Spieltag implements SpieltagAllg {

	private String jahr;
	private int spieltag;

	private List<SpielAllg> spiele;

	@Override
	public void setJahr(String jahr) {
		this.jahr = jahr;
	}

	@Override
	public void setSpieltag(int spieltag) {
		this.spieltag = spieltag;
	}

	@Override
	public void setSpiele(List<SpielAllg> spiele) {
		this.spiele = spiele;
	}

	@Override
	public AuswertungAllg auswerten() {
		Auswertung auswertung = new Auswertung();
		auswertung.setJahr(jahr);
		auswertung.setSpieltag(spieltag);

		List<SpielAuswertungAllg> spielauswertungen = spiele.stream().map(s -> s.auswerten())
				.collect(Collectors.toList());
		auswertung.setSpielauswertungen(spielauswertungen);

		auswertung.setGesamtpunkteBeiUser(berechneGesamtpunkteBeiUser(spielauswertungen));

		auswertung.setGesamtpunkte(berechneGesamtpunkte(spielauswertungen));

		return auswertung;
	}

	private Matrix<User, User, Double> berechneGesamtpunkteBeiUser(List<SpielAuswertungAllg> spielauswertungen) {

		Matrix<User, User, Double> spieltagPunkteBeiUser = new Matrix<>();

		for (SpielAuswertungAllg spielauswertung : spielauswertungen) {
			spieltagPunkteBeiUser.addition(spielauswertung.getPunkte(), Double::sum);
		}

		return spieltagPunkteBeiUser;
	}

	private Map<User, Double> berechneGesamtpunkte(List<SpielAuswertungAllg> auswertungen) {

		EnhancedMap<User, Double> gesamtpunkteSpieltag = new EnhancedMap<>();

		for (SpielAuswertungAllg auswertung : auswertungen) {

			Map<User, Double> gesamtpunkte = auswertung.getGesamtpunkte();

			for (Entry<User, Double> entry : gesamtpunkte.entrySet()) {
				gesamtpunkteSpieltag.put(entry.getKey(), entry.getValue(), Double::sum);
			}
		}
		return gesamtpunkteSpieltag;
	}

}
