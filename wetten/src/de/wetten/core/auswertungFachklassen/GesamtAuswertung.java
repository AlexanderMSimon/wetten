package de.wetten.core.auswertungFachklassen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public class GesamtAuswertung extends GesamtAuswertungAllg {

	private String jahr;
	private List<Integer> spieltage;
	private Matrix<User, User, Double> gesamtpunkteBeiUser = new Matrix<>();
	private Map<User, Double> gesamtpunkte = new HashMap<>();
	private Map<Integer, Double> awkwardnessProSpieltag = new HashMap<>();

	public GesamtAuswertung(List<AuswertungAllg> auswertungen) {
		super(auswertungen);

		setzeJahr(auswertungen);

		setzeSpieltage(auswertungen);

		setzeGesamtpunkteBeiUser(auswertungen);

		setzeGesamtpunkte(auswertungen);

		setzeAwkwardnessProSpieltag(auswertungen);
	}

	private void setzeAwkwardnessProSpieltag(List<AuswertungAllg> auswertungen) {

		for (AuswertungAllg auswertung : auswertungen) {
			Double awkwardness = auswertung.getSpielauswertungen().stream().map(s -> s.getAwkwardness()).reduce(1.0,
					(a, b) -> a * b);

			awkwardnessProSpieltag.put(auswertung.getSpieltag(), awkwardness);
		}
	}

	private void setzeGesamtpunkte(List<AuswertungAllg> auswertungen) {
		List<Map<User, Double>> maps = auswertungen.stream().map(a -> a.getGesamtpunkte()).collect(Collectors.toList());

		for (Map<User, Double> map : maps) {
			for (Entry<User, Double> entry : map.entrySet()) {

				User user = entry.getKey();
				Double valueAlt = gesamtpunkte.get(entry.getKey());

				if (valueAlt == null) {
					gesamtpunkte.put(user, entry.getValue());
				} else {
					double value = valueAlt + entry.getValue();
					gesamtpunkte.put(user, value);
				}
			}
		}
	}

	private void setzeGesamtpunkteBeiUser(List<AuswertungAllg> auswertungen) {
		List<SpielAuswertungAllg> punkte = new ArrayList<>();
		auswertungen.stream().forEach(a -> punkte.addAll(a.getSpielauswertungen()));

		List<Matrix<User, User, Double>> matrizen = punkte.stream().map(p -> p.getPunkte())
				.collect(Collectors.toList());

		for (User user : User.values()) {
			for (Matrix<User, User, Double> matrix : matrizen) {

				HashMap<User, Double> byUser = matrix.getByKey1(user);

				if (byUser != null) {
					byUser.entrySet().stream()
							.forEach(e -> gesamtpunkteBeiUser.append(user, e.getKey(), e.getValue(), Double::sum));
				}
			}
		}
	}

	private void setzeSpieltage(List<AuswertungAllg> auswertungen) {
		List<Integer> spieltage = auswertungen.stream().map(a -> a.getSpieltag()).collect(Collectors.toList());

		if (containsDuplicates(spieltage)) {
			throw new RuntimeException("Problem mit den Daten: Spieltage");
		}
		this.spieltage = spieltage;
	}

	private boolean containsDuplicates(List<Integer> list) {
		return new HashSet<>(list).size() < list.size();
	}

	private void setzeJahr(List<AuswertungAllg> auswertungen) {
		List<String> jahre = auswertungen.stream().map(a -> a.getJahr()).distinct().collect(Collectors.toList());

		if (jahre.size() != 1) {
			throw new RuntimeException("Problem mit den Daten: Jahre");
		}

		this.jahr = jahre.get(0);
	}

	@Override
	public String getJahr() {
		return jahr;
	}

	@Override
	public List<Integer> getSpieltage() {
		return spieltage;
	}

	@Override
	public Matrix<User, User, Double> getGesamtpunkteBeiUser() {
		return gesamtpunkteBeiUser;
	}

	@Override
	public Map<User, Double> getGesamtpunkte() {
		return gesamtpunkte;
	}

	@Override
	public Map<Integer, Double> getAwkwardnessProSpieltag() {
		return awkwardnessProSpieltag;
	}

}
