package de.wetten.core.auswertungFachklassen;

import java.util.Map;

import de.wetten.core.fachklassen.Resultat;
import de.wetten.core.fachklassen.Verein;
import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public interface SpielAuswertungAllg {

	Verein getHeim();

	Verein getGast();

	Resultat getResultat();

	Matrix<User, Resultat, Integer> getProzente();

	Matrix<User, Resultat, Double> getQuoten();

	Matrix<User, User, Resultat> getWetten();

	Matrix<User, User, Double> getPunkte();

	Map<User, Double> getGesamtpunkte();

	Double getAwkwardness();

}
