package de.wetten.core.auswertungFachklassen;

import java.util.List;
import java.util.Map;

import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public abstract class GesamtAuswertungAllg {

	public GesamtAuswertungAllg(List<AuswertungAllg> auswertungen) {
	}

	public abstract String getJahr();

	public abstract List<Integer> getSpieltage();

	public abstract Matrix<User, User, Double> getGesamtpunkteBeiUser();

	public abstract Map<User, Double> getGesamtpunkte();

	public abstract Map<Integer, Double> getAwkwardnessProSpieltag();
}
