package de.wetten.core.auswertungFachklassen;

import java.util.List;
import java.util.Map;

import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public class Auswertung implements AuswertungAllg {
	private String jahr;
	private int spieltag;

	private List<SpielAuswertungAllg> spielauswertungen;
	private Matrix<User, User, Double> gesamtpunkteBeiUser;;
	private Map<User, Double> gesamtpunkte;

	// --- Interface ---
	@Override
	public String getJahr() {
		return jahr;
	}

	@Override
	public int getSpieltag() {
		return spieltag;
	}

	@Override
	public List<SpielAuswertungAllg> getSpielauswertungen() {
		return spielauswertungen;
	}

	@Override
	public Matrix<User, User, Double> getGesamtpunkteBeiUser() {
		return gesamtpunkteBeiUser;
	}

	@Override
	public Map<User, Double> getGesamtpunkte() {
		return gesamtpunkte;
	}

	// --- intern ---

	public void setJahr(String jahr) {
		this.jahr = jahr;
	}

	public void setSpieltag(int spieltag) {
		this.spieltag = spieltag;
	}

	public void setSpielauswertungen(List<SpielAuswertungAllg> spielauswertungen) {
		this.spielauswertungen = spielauswertungen;
	}

	public void setGesamtpunkteBeiUser(Matrix<User, User, Double> gesamtpunkteBeiUser) {
		this.gesamtpunkteBeiUser = gesamtpunkteBeiUser;
	}

	public void setGesamtpunkte(Map<User, Double> gesamtpunkte) {
		this.gesamtpunkte = gesamtpunkte;
	}

}
