package de.wetten.core.auswertungFachklassen;

import java.util.List;
import java.util.Map;

import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public interface AuswertungAllg {

	String getJahr();

	int getSpieltag();

	List<SpielAuswertungAllg> getSpielauswertungen();

	Matrix<User, User, Double> getGesamtpunkteBeiUser();

	Map<User, Double> getGesamtpunkte();

}
