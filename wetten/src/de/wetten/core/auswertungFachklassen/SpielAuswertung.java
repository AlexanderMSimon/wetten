package de.wetten.core.auswertungFachklassen;

import java.util.Map;

import de.wetten.core.fachklassen.Resultat;
import de.wetten.core.fachklassen.Verein;
import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public class SpielAuswertung implements SpielAuswertungAllg {

	private Verein heim;
	private Verein gast;
	private Resultat resultat;

	private Matrix<User, Resultat, Integer> prozentMatrix;

	private Matrix<User, Resultat, Double> quoten;
	private Matrix<User, User, Resultat> wetten;
	private Matrix<User, User, Double> punkte;
	private Map<User, Double> gesamtpunkte;
	private Double awkwardness;

	public SpielAuswertung(Verein heim, Verein gast) {
		this.heim = heim;
		this.gast = gast;
	}

	// --- Interface ---

	@Override
	public Verein getHeim() {
		return heim;
	}

	@Override
	public Verein getGast() {
		return gast;
	}

	@Override
	public Resultat getResultat() {
		return resultat;
	}

	@Override
	public Matrix<User, Resultat, Integer> getProzente() {
		return prozentMatrix;
	}

	@Override
	public Matrix<User, Resultat, Double> getQuoten() {
		return quoten;
	}

	@Override
	public Matrix<User, User, Resultat> getWetten() {
		return wetten;
	}

	@Override
	public Matrix<User, User, Double> getPunkte() {
		return punkte;
	}

	@Override
	public Map<User, Double> getGesamtpunkte() {
		return gesamtpunkte;
	}

	@Override
	public Double getAwkwardness() {
		return awkwardness;
	}

	// --- intern ---

	public void setResultat(Resultat resultat) {
		this.resultat = resultat;
	}

	public void setProzente(Matrix<User, Resultat, Integer> prozentMatrix) {
		this.prozentMatrix = prozentMatrix;
	}

	public void setQuoten(Matrix<User, Resultat, Double> quoten) {
		this.quoten = quoten;
	}

	public void setWetten(Matrix<User, User, Resultat> wetten) {
		this.wetten = wetten;
	}

	public void setPunkte(Matrix<User, User, Double> punkte) {
		this.punkte = punkte;
	}

	public void setGesamtpunkte(Map<User, Double> gesamtpunkte) {
		this.gesamtpunkte = gesamtpunkte;
	}

	public void setAwkwardness(Double awkwardness) {
		this.awkwardness = awkwardness;
	}

}
