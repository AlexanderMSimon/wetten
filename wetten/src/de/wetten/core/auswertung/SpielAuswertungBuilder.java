package de.wetten.core.auswertung;

import static de.wetten.konfiguration.Konfigurationen.AWKWARDNESS_USER;
import static de.wetten.konfiguration.Konfigurationen.MODUS;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.wetten.core.auswertungFachklassen.SpielAuswertung;
import de.wetten.core.auswertungFachklassen.SpielAuswertungAllg;
import de.wetten.core.fachklassen.Resultat;
import de.wetten.core.fachklassen.Verein;
import de.wetten.konfiguration.Modus;
import de.wetten.konfiguration.User;
import de.wetten.util.Matrix;

public class SpielAuswertungBuilder {

	private static final String fehlerMeldung = "Fehlerhafter Zustand bei der Berechnung der Wettplatzierung";

	private static final double einsatz = -1.0;

	private Verein heim;
	private Verein gast;
	private Resultat resultat;
	private Matrix<User, Resultat, Integer> prozentMatrix;

	public SpielAuswertungBuilder setHeim(Verein heim) {
		this.heim = heim;
		return this;
	}
	public SpielAuswertungBuilder setGast(Verein gast) {
		this.gast = gast;
		return this;
	}
	public SpielAuswertungBuilder setResultat(Resultat resultat) {
		this.resultat = resultat;
		return this;
	}
	public SpielAuswertungBuilder setProzente(Matrix<User, Resultat, Integer> prozentMatrix) {
		this.prozentMatrix = prozentMatrix;
		return this;
	}

	public SpielAuswertungAllg build() {

		validiereProzente();

		// setze bekannte Werte
		SpielAuswertung auswertung = new SpielAuswertung(heim, gast);
		auswertung.setResultat(resultat);
		auswertung.setProzente(prozentMatrix);

		// berechne Werte
		Matrix<User, Resultat, Double> quoten = prozenteToQuoten();
		Matrix<User, User, Resultat> wetten = quotenToWetten(quoten);
		Matrix<User, User, Double> punkte = wettenToPunkte(wetten, quoten);
		Map<User, Double> gesamtpunkte = punkteToGesamtpunkte(punkte);

		// setze Werte
		auswertung.setQuoten(quoten);
		auswertung.setWetten(wetten);
		auswertung.setPunkte(punkte);
		auswertung.setGesamtpunkte(gesamtpunkte);

		// awkwardness
		auswertung.setAwkwardness(ermittleAwkwardness());

		return auswertung;
	}

	private Double ermittleAwkwardness() {
		if (resultat == null) {
			return 0.0;
		}

		Map<Resultat, Integer> prozentMap = prozentMatrix.getByKey1(AWKWARDNESS_USER);

		int maxprozent = prozentMap.values().stream().mapToInt(Integer::intValue).max().orElseThrow(
				() -> new IllegalStateException("Fehlerhafter Zustand bei der Berechnung der Awkwardness"));

		return maxprozent / (double) prozentMap.get(resultat);
	}

	/**
	 * Fuer jedes Resultat sind Prozente angegeben. Die Zahlen sind alle
	 * positiv. Die Summe ist 100.
	 */
	private void validiereProzente() {

		if (prozentMatrix == null) {
			throw new IllegalStateException("prozentMatrix ist null");
		}

		Set<User> users = prozentMatrix.keySet1();

		for (User user : users) {
			int summe = 0;

			for (Resultat resultat : Resultat.values()) {
				Integer prozent = prozentMatrix.get(user, resultat);

				if (prozent == null) {
					throw new IllegalStateException(
							"User " + user + " hat nicht fuer alle Resultate eine Prozentzahl angegeben.");
				}

				if (prozent < 0) {
					throw new IllegalStateException("User " + user + " hat eine negative Prozentzahl angegeben.");
				}
				summe += prozent;
			}

			if (summe != 100) {
				throw new IllegalStateException("Die Prozente von User " + user + " ergeben nicht 100.");
			}
		}
	}

	private Matrix<User, Resultat, Double> prozenteToQuoten() {
		Matrix<User, Resultat, Double> quoten = new Matrix<>();

		for (User user : prozentMatrix.keySet1()) {

			for (Resultat resultat : Resultat.values()) {
				Double quote = 100.0 / prozentMatrix.get(user, resultat);
				quoten.put(user, resultat, quote);
			}
		}

		return quoten;
	}

	private Matrix<User, User, Resultat> quotenToWetten(Matrix<User, Resultat, Double> quoten) {
		Matrix<User, User, Resultat> wetten = new Matrix<>();

		for (User user : prozentMatrix.keySet1()) {
			for (User beiUser : quoten.keySet1()) {

				if (beiUser.equals(user)) {
					continue;
				}
				Resultat wettresultat = ermittleWettresultat(user, beiUser, quoten);

				wetten.put(user, beiUser, wettresultat);
			}
		}
		return wetten;
	}

	/**
	 * Ermittelt, auf welches Resultat user beiUser wettet.
	 */
	private Resultat ermittleWettresultat(User user, User beiUser, Matrix<User, Resultat, Double> quoten) {

		List<Resultat> wettresultate = filterMaxEw(user, beiUser, quoten);

		if (wettresultate.size() == 1) {
			return wettresultate.get(0);
		}

		// --- falls mehr als eins uebrig ---

		wettresultate = filterMaxProzente(wettresultate, user);

		if (wettresultate.size() == 1) {
			return wettresultate.get(0);
		}

		// --- falls mehr als eins uebrig ---

		wettresultate.sort(new ResultatComparator());

		return wettresultate.get(0);
	}

	/**
	 * Filtert nach dem besten Erwartungswert, den User beiUser erzielen kann.
	 */
	private List<Resultat> filterMaxEw(User user, User beiUser, Matrix<User, Resultat, Double> quoten) {

		Function<Resultat, Double> ewMapper = r -> prozentMatrix.get(user, r) * quoten.get(beiUser, r);

		double maxEw = Arrays.stream(Resultat.values()).map(ewMapper).mapToDouble(Double::doubleValue).max()
				.orElseThrow(() -> new IllegalStateException(fehlerMeldung));

		return Arrays.stream(Resultat.values()).filter(r -> ewMapper.apply(r).equals(maxEw))
				.collect(Collectors.toList());
	}

	/**
	 * Filtert nach der hoeschsten Prozentzahl, die User den Resultaten vergeben
	 * hat.
	 */
	private List<Resultat> filterMaxProzente(List<Resultat> resultate, User user) {

		Function<Resultat, Integer> prozentMapper = r -> prozentMatrix.get(user, r);

		int maxProzent = resultate.stream().map(prozentMapper).mapToInt(Integer::intValue).max()
				.orElseThrow(() -> new IllegalStateException(fehlerMeldung));

		return resultate.stream().filter(r -> prozentMapper.apply(r) == maxProzent).collect(Collectors.toList());
	}

	private Matrix<User, User, Double> wettenToPunkte(Matrix<User, User, Resultat> wetten,
			final Matrix<User, Resultat, Double> quoten) {

		Matrix<User, User, Double> punkte = new Matrix<>();

		if (resultat == null) {
			return punkte;
		}

		for (User user : wetten.keySet1()) {
			HashMap<User, Resultat> wettenDesUsers = wetten.getByKey1(user);

			gewinnAuszahlen(user, wettenDesUsers, quoten, punkte);

			if (Modus.EINSATZ.equals(MODUS)) {
				einsatzKassieren(user, wettenDesUsers, punkte);

			}
		}
		return punkte;
	}

	/**
	 * Zieht bei allen Wetten einen Punkt als Einsatz ab.
	 */
	private void einsatzKassieren(User user, HashMap<User, Resultat> wettenDesUsers,
			Matrix<User, User, Double> punkte) {
		for (Entry<User, Resultat> wette : wettenDesUsers.entrySet()) {
			User beiUser = wette.getKey();

			punkte.append(user, beiUser, einsatz, Double::sum);
		}
	}

	/**
	 * Fuegt zu den Punkten den Gewinn (einschliesslich der Rueckerstattung des
	 * Eingesetzten Betrags) hinzu.
	 */
	private void gewinnAuszahlen(User user, HashMap<User, Resultat> wettenDesUsers,
			final Matrix<User, Resultat, Double> quoten, Matrix<User, User, Double> punkte) {
		List<Entry<User, Resultat>> gewonneneWettenDesUsers = wettenDesUsers.entrySet().stream()
				.filter(e -> e.getValue().equals(resultat)).collect(Collectors.toList());

		// gewonneneWettenDesUsers: beiUser -- Resultat
		for (Entry<User, Resultat> gewonneneWette : gewonneneWettenDesUsers) {

			User beiUser = gewonneneWette.getKey();
			Double gewinn = quoten.get(beiUser, gewonneneWette.getValue());

			punkte.append(user, beiUser, gewinn, Double::sum);
		}
	}

	private Map<User, Double> punkteToGesamtpunkte(Matrix<User, User, Double> punkte) {
		Map<User, Double> gesamtpunkte = new HashMap<>();

		for (User user : punkte.keySet1()) {
			Double summe = punkte.getByKey1(user).values().stream().reduce(0.0, Double::sum);
			gesamtpunkte.put(user, summe);
		}
		return gesamtpunkte;
	}

}
