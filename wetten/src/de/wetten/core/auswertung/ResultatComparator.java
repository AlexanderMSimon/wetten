package de.wetten.core.auswertung;

import java.util.Comparator;

import de.wetten.core.fachklassen.Resultat;

public class ResultatComparator implements Comparator<Resultat> {

	@Override
	public int compare(Resultat r1, Resultat r2) {
		return value(r1) - value(r2);
	}

	/**
	 * Je kleiner die Zahl, desto weiter vorne landet das Resultat.
	 * 
	 * @param resultat
	 * @return
	 */
	private int value(Resultat resultat) {
		switch (resultat) {
			case HEIM :
				return 10;

			case GAST :
				return 20;

			case REMIS :
				return 30;

			default :
				throw new IllegalArgumentException("unbekanntes Resultat");
		}
	}

}
