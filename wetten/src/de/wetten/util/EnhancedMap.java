package de.wetten.util;

import java.util.HashMap;
import java.util.function.BinaryOperator;

public class EnhancedMap<K, V> extends HashMap<K, V> {

	private static final long serialVersionUID = 1L;

	public V put(K key, V value, BinaryOperator<V> accummulator) {

		V newValue = get(key) == null ? value : accummulator.apply(get(key), value);

		put(key, newValue);
		return newValue;
	}

}
