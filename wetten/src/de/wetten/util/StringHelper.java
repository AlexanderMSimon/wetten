package de.wetten.util;

import java.text.DecimalFormat;
import java.util.Locale;

public class StringHelper {

	private StringHelper() {
	}

	public static String doubleToString(Double d) {
		Locale.setDefault(new Locale("en", "US"));
		DecimalFormat df = new DecimalFormat("#.00");

		String format = df.format(d == null ? 0.0 : d);
		return format;
	}
}
