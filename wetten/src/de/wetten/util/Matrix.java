package de.wetten.util;

import java.util.HashMap;
import java.util.Set;
import java.util.function.BinaryOperator;

// zwei Schluessel und ein Wert
public class Matrix<K1, K2, V> {

	private HashMap<K1, HashMap<K2, V>> map = new HashMap<>();

	public void put(K1 key1, K2 key2, V value) {

		if (map == null) {
			map = new HashMap<>();
		}

		HashMap<K2, V> innerMap = map.get(key1);

		if (innerMap == null) {
			innerMap = new HashMap<>();
		}

		innerMap.put(key2, value);
		map.put(key1, innerMap);
	}

	/**
	 * Matrixaddition mit concatOperator als Additionsoperation auf V
	 */
	public void addition(Matrix<K1, K2, V> matrix, BinaryOperator<V> concatOperator) {

		for (K1 k1 : matrix.keySet1()) {
			HashMap<K2, V> map = matrix.getByKey1(k1);

			for (K2 k2 : map.keySet()) {
				append(k1, k2, matrix.get(k1, k2), concatOperator);
			}
		}
	}

	public void append(K1 key1, K2 key2, V value, BinaryOperator<V> concatOperator) {
		V oldValue = get(key1, key2);

		if (oldValue == null) {
			put(key1, key2, value);

		} else {
			V newValue = concatOperator.apply(oldValue, value);
			put(key1, key2, newValue);
		}
	}

	public V get(K1 key1, K2 key2) {

		if (map == null) {
			map = new HashMap<>();
		}

		HashMap<K2, V> innerMap = map.get(key1);

		if (innerMap == null) {
			return null;
		}

		return innerMap.get(key2);
	}

	public HashMap<K2, V> getByKey1(K1 key1) {
		return map.get(key1);
	}

	public Set<K1> keySet1() {
		return map.keySet();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (K1 key1 : keySet1()) {

			sb.append(key1.toString() + " : " + getByKey1(key1));
			sb.append(System.getProperty("line.separator"));
		}

		return sb.toString();
	}

}
