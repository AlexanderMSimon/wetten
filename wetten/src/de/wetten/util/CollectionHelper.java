package de.wetten.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class CollectionHelper {

	private CollectionHelper() {
	}

	public static <K, V> List<V> getSortedValues(Map<K, V> map) {
		return map.values().stream().sorted().collect(Collectors.toList());
	}

	public static <K, V> K getAnyMatchingKeyByValue(Map<K, V> map, V value) {
		for (Entry<K, V> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	public static <K, V> List<K> getKeysSortedByValues(Map<K, V> map) {

		List<K> result = new ArrayList<>();

		List<V> sortedValues = getSortedValues(map);

		for (V value : sortedValues) {
			K key = getAnyMatchingKeyByValue(map, value);
			result.add(key);
		}
		return result;
	}

}
