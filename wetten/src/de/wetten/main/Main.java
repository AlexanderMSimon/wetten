package de.wetten.main;

import static de.wetten.konfiguration.Konfigurationen.JAHR;
import static de.wetten.konfiguration.Konfigurationen.getAuswertungsConsumers;
import static de.wetten.konfiguration.Konfigurationen.getGesamtAuswertungsConsumers;
import static de.wetten.konfiguration.Konfigurationen.getSpieltage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.wetten.core.auswertungFachklassen.AuswertungAllg;
import de.wetten.core.auswertungFachklassen.GesamtAuswertung;
import de.wetten.in.init.BundesligaReader;
import de.wetten.in.init.SpieltagInitiator;

public class Main {
	public static void main(String[] args) {

		List<AuswertungAllg> auswertungen = new ArrayList<>();

		for (int spieltag : getSpieltage()) {
			SpieltagInitiator initiator = new BundesligaReader(JAHR, spieltag);

			AuswertungAllg auswertung = initiator.init().auswerten();

			Arrays.stream(getAuswertungsConsumers()).forEach(c -> c.consume(auswertung));

			auswertungen.add(auswertung);
		}

		GesamtAuswertung gesamtAuswertung = new GesamtAuswertung(auswertungen);
		Arrays.stream(getGesamtAuswertungsConsumers()).forEach(c -> c.consume(gesamtAuswertung));
	}
}
